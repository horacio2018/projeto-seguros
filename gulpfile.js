var gulp   = require('gulp');
var sass   = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');


gulp.task('imagemin',function(){
    gulp.src('./src/imagens/*')
    .pipe(imagemin(
        [
        imagemin.gifsicle({interlaced: true}),
        imagemin.jpegtran({progressive: true}),
        imagemin.optipng({optimizationLevel: 5}),
        imagemin.svgo({
            plugins: [
                {removeViewBox: true},
                {cleanupIDs: false}
            ]
            })
        ]
    ))
    .pipe(gulp.dest('./dist/imagens/'))
});

gulp.task('sass',function(){
   gulp.src('./src/css/*').pipe(sass({outputStyle:"compressed"})).pipe(gulp.dest('./dist/css/'))
   //gulp.src('./src/css/estilo992.scss').pipe(sass({outputStyle:"compressed"})).pipe(gulp.dest('./dist/css/'))

});


gulp.task('js',function(){
    return gulp.src('./src/js/*.js')
    .pipe(concat('all.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist/js/'));
});


gulp.task('default',['sass','js'],function(){
    gulp.watch('./src/css/*.scss',['sass']);
    gulp.watch('./src/css/base/*.scss',['sass']);
    gulp.watch('./src/js/*.js',['js']);
});
