<?php
  ob_start();
  define("tema", get_theme_file_uri());
?>

<html  <?php language_attributes(); ?> class="no-js no-svg" >
<head>
<link rel="stylesheet" href="<?php echo tema;?>/css/reset.css">
  <link rel="stylesheet" href="<?php echo tema;?>/dist/css/menu.css">
  <link rel="stylesheet" href="<?php echo tema;?>/dist/css/apresentacao.css">
  <link rel="stylesheet" href="<?php echo tema;?>/dist/css/produtos.css">
  <link rel="stylesheet" href="<?php echo tema;?>/dist/css/formularioCTA.css">
  <link rel="stylesheet" href="<?php echo tema;?>/dist/css/rodape.css">
  <?php wp_head();?>
</head>
<body>
<!--------------------------->
<?php get_template_part("partes/menu"); ?>
<!--------------------------->
<?php get_template_part("partes/topo"); ?>
<!--------------------------->
<?php get_template_part("partes/produtos"); ?>
<!--------------------------->
<?php get_template_part("partes/cta-meio"); ?>
<!--------------------------->
<?php get_template_part("partes/footer"); ?>
<!--------------------------->
<script src="<?php echo tema?>/dist/js/all.js"></script>
<?php wp_footer();?>
</body>
</html>
